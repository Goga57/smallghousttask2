﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace smallGhoustSecondTask
{
    class Program
    {
        static void Main(string[] args)
        {
            Double x = 7;
            List<Double> testCollection = new List<Double> {7,8,7,4,5,3,8,9,2,5,4,1,6,-5,12,-4,10,42,-3,6,0};

            List<Tuple<Double, Double>>rez = GetPairsVLOB(x, testCollection);
            PrintRez(x,rez);

            List<Tuple<Double, Double>> rezSecond = GetPairsSecondTry(x, testCollection);
            PrintRez(x, rezSecond);

            List<Tuple<Double, Double>> rezThird = GetPairsThirdTry(x, testCollection);
            PrintRez(x, rezThird);
        }

        private static void PrintRez(double x,List<Tuple<Double, Double>> rez)
        {
            Console.WriteLine("result is ");

            foreach (var pair in rez)
            {
                Console.WriteLine("x is {0} pair is {1} and {2}", x, pair.Item1, pair.Item2);
                
            }
            Console.ReadLine();
        }


        // O(n^2)
        private static List<Tuple<Double, Double>> GetPairsVLOB(Double x, List<Double> inCollection)
        {
            List<Tuple<Double, Double>> retList = new List<Tuple<double, double>>();

            foreach (var itemOuter in inCollection)
            {
                foreach (var itemInner in inCollection)
                {
                    if (itemInner + itemOuter == x)
                        retList.Add(new Tuple<Double, Double>(itemInner, itemOuter));
                }
            }

            return retList;
        }

        private static List<Tuple<Double, Double>> GetPairsSecondTry(Double x, List<Double> inCollection)
        {
            List<Tuple<Double, Double>> retList = new List<Tuple<double, double>>();

            //sort list by des O (n log n)
            var querySort=inCollection.OrderByDescending(i => i);
            var sortedLs = querySort.ToList();
            var smallestEl = sortedLs.Last();

            // get all m elements that in theory may be a pair
            // O(n) current complexity still  O (n log n)
            var queryFind = sortedLs.Find(el => el <= x - smallestEl);
            var workList = sortedLs.ToList();

            // теперь если решить задачу "в лоб", то при пессимистичном случае опять получим
            // O(n^2) при оптимистичном O(m^2) квадрат где m<n
            retList=GetPairsVLOB(x, workList);

            return retList;
        }

        private static List<Tuple<Double, Double>> GetPairsThirdTry(Double x, List<Double> inCollection)
        {
            List<Tuple<Double, Double>> retList = new List<Tuple<double, double>>();

            //sort list by desc O(n log n)
            var querySort = inCollection.OrderByDescending(i => i);
            var sortedLs = querySort.ToList();
            var smallestEl = sortedLs.Last();

            // get all m elements that in theory may be a pair
            // O(n) current complexity still  O(n log n)
            var queryFind = sortedLs.Find(el => el <= x - smallestEl);
            var workList = sortedLs.ToList();


            // забыли о варианте "в лоб"
            // 

            var biggestEl = workList.First();

            while (biggestEl > smallestEl)
            {
                if (biggestEl + smallestEl == x)
                {
                    retList.Add(new Tuple<double, double>(biggestEl, smallestEl));
                    workList.Remove(biggestEl);
                    biggestEl = workList.First();
                }
                else
                {
                    if (biggestEl + smallestEl > x)
                    {
                        workList.Remove(biggestEl);
                        biggestEl = workList.First();
                    }
                    else
                    {
                        workList.Remove(smallestEl);
                        smallestEl = workList.Last();
                    }
                }
            }



            return retList;
        }
    }
}
